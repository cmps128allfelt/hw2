package mainInstance

import (
	"log"
	"net/http"
    "fmt"
	"github.com/gorilla/mux"
    "kvsAccess"
    "encoding/json"

)

var _KVS *kvsAccess.KeyValStore

func Start() {
    router := mux.NewRouter()

    //init instance of our global kvs
    _KVS = kvsAccess.NewKVS()

    router.HandleFunc("/kvs", Set).Methods("POST", "PUT")
    router.HandleFunc("/kvs", Get).Methods("GET")
    router.HandleFunc("/kvs", Del).Methods("DELETE")

    if err := http.ListenAndServe(":8080", router); err != nil {
        log.Fatal(err)
    }
}

func keyValid(key string) bool {
    keyLen := len(key)
    if (keyLen > 250 || keyLen < 1){
        return false
    }
    return true
}

func Set(w http.ResponseWriter, r *http.Request) {
    var resp string
	
    r.ParseForm()
    key := r.PostFormValue("key")

    value := r.PostFormValue("value")

	put := PUT{}

    if ( len(key) == 0 || len(value) == 0 ) {
				put.Message = "error"
				put.Replaced = 0
				w.WriteHeader(400)
    } else {
		if !keyValid(key) {
			put.Message = "error"
			put.Replaced = 0
			w.WriteHeader(401)
		} else {

	        resp = _KVS.SetValue(key, value)

			 w.Header().Set("Content-Type","application/json")

		    put.Message = "success"

		    if (resp == "") {
		        w.WriteHeader(201)
		        put.Replaced = 0
		    }else{
		        w.WriteHeader(200)
		        put.Replaced = 1
		    }
		}
	}

    jsonResponse, err := json.Marshal(put)
    if err != nil{
        panic(err)
    }
    w.Write(jsonResponse)
}

func Get(w http.ResponseWriter, r *http.Request) {
    key,   keyExists := r.URL.Query()["key"]

    if !keyExists {
        fmt.Fprintf(w, "no key in query")
        return
    }
    
    var resp string
    resp = _KVS.GetValue(key[0])

    w.Header().Set("Content-Type","application/json")

    if (resp == "") {
        w.WriteHeader(404)
        get := ERROR{"error","key does not exist"}
        jsonResponse, err := json.Marshal(get)
        if err != nil{
            panic(err)
        }
        w.Write(jsonResponse)
    } else {
        w.WriteHeader(200)
        get := GETSuccess{"success", resp}
        jsonResponse, err := json.Marshal(get)
        if err != nil{
            panic(err)
        }
        w.Write(jsonResponse)
    }


}

func Del(w http.ResponseWriter, r *http.Request) {
    key,   keyExists := r.URL.Query()["key"]

    if !keyExists {
        fmt.Fprintf(w, "no key in query")
        return
    }

    var resp string
    resp = _KVS.DelValue(key[0])

    w.Header().Set("Content-Type","application/json")

    if (resp == "") {
        w.WriteHeader(404)
        get := ERROR{"error","key does not exist"}
        jsonResponse, err := json.Marshal(get)
        if err != nil{
            panic(err)
        }
        w.Write(jsonResponse)
    } else {
        w.WriteHeader(200)
        get := DELExists{"success"}
        jsonResponse, err := json.Marshal(get)
        if err != nil{
            panic(err)
        }
        w.Write(jsonResponse)
    }

}


type GETSuccess struct {
   Message string `json:"msg"`
   Value string `json:"value"`
}
type PUT struct {
    Replaced int `json:"replaced"`
    Message string `json:"msg"`
}
type ERROR struct {
    Message string `json:"msg"`
    Error string `json:"error"`
}
type DELExists struct {
    Message string `json:"msg"`
}
