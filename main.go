package main

import (
    "mainInstance"
    "forwarderInstance"
    "os"
)

func main() {
	ip := os.Getenv("MAINIP")

    if (ip!="") {
        forwarderInstance.Start(ip)
    } else {
        mainInstance.Start()
    }
}
